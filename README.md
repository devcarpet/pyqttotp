# PyQtTOTP

## Description

PyQtTOTP is a TOTP QR code generator.

## Usage

```
pip install -r requirements.txt
python src/pyqttotp.py
```
## Mini Roadmap

* Add import from few popular TOTP tools (useful during the migration to the other tool).
* Create pip package.

## Screenshot

![Screenshot](images/screenshot00.png)
